﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KinectClouds.KinectFramework
{
    class Coordinate2D
    {
        public double X;
        public double Y;

        public Coordinate2D(double x, double y)
        {
            X = x;
            Y = y;
        }
    }
}
