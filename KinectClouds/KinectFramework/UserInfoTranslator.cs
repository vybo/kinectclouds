﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using HelixToolkit.Wpf;
using Microsoft.Kinect;
using Microsoft.Kinect.Toolkit;
using Microsoft.Kinect.Toolkit.Controls;
using Microsoft.Kinect.Toolkit.Interaction;
using System.Collections.ObjectModel;
using HelixToolkit.Wpf.SharpDX;
using HelixToolkit.Wpf.SharpDX.Core;

namespace KinectClouds.KinectFramework
{
    class UserInfoTranslator
    {
        private HelixViewport3D _helix = new HelixViewport3D();
        private Viewport3DX _sharp = new Viewport3DX();
        private System.Windows.Controls.Control _view;
        private DebugWindow debugWindow;
        private KinectRegion _kinectRegion;
        private bool debug = false;

        private Dictionary<int, InteractionHandEventType> _lastLeftHandEvents = new Dictionary<int, InteractionHandEventType>();
        private Dictionary<int, InteractionHandEventType> _lastRightHandEvents = new Dictionary<int, InteractionHandEventType>(); // nejak do listu ci co

        private Dictionary<int, Coordinate2D> _leftHandGripPosition = new Dictionary<int, Coordinate2D>();
        private Dictionary<int, Coordinate2D> _rightHandGripPosition = new Dictionary<int, Coordinate2D>();

        // THIS CLASS WILL TRANSLATE USERINFO INTO GESTURES, POSSIBLY WILL EVEN CONTROL THE VIEWPORT
        public UserInfoTranslator(System.Windows.Controls.Control viewport, KinectRegion kinectRegion)
        {
            SetViewport(viewport);
            _kinectRegion = kinectRegion;

            if (debug)
            {
                debugWindow = new DebugWindow();
                debugWindow.Show();
            }
        }

        public void SetViewport(System.Windows.Controls.Control viewport)
        {
            _view = viewport;

            if (_view.GetType() == typeof(HelixViewport3D))
            {
                _helix = (HelixViewport3D)_view;
            }
            else if (_view.GetType() == typeof(Viewport3DX))
            {
                _sharp = (Viewport3DX)_view;
            }
        }

        public void ProcessUserInfo(UserInfo[] userInfos)
        {
            StringBuilder dump = new StringBuilder();

            bool hasUser = false;
            
            foreach (UserInfo userInfo in userInfos)
            {
                Coordinate2D dragVector = new Coordinate2D(0, 0);
                Coordinate2D newLeftHandDragVector = new Coordinate2D(0, 0);
                Coordinate2D newRightHandDragVector = new Coordinate2D(0, 0);

                int userID = userInfo.SkeletonTrackingId;
                
                if (userID == 0)
                    continue;

                hasUser = true;
                dump.AppendLine("User ID = " + userID);
                dump.AppendLine("  Hands: ");
                ReadOnlyCollection<InteractionHandPointer> hands = userInfo.HandPointers;
                
                if (hands.Count == 0)
                {
                    dump.AppendLine("    No hands");
                }
                else
                {
                    foreach (InteractionHandPointer hand in hands)
                    {
                        var lastHandEvents = hand.HandType == InteractionHandType.Left
                                                 ? _lastLeftHandEvents
                                                 : _lastRightHandEvents;

                        var lastHandGripPosition = hand.HandType == InteractionHandType.Left
                                                ? _leftHandGripPosition
                                                : _rightHandGripPosition;

                        if (hand.HandEventType != InteractionHandEventType.None)
                        {
                            lastHandEvents[userID] = hand.HandEventType;

                            if (hand.HandEventType == InteractionHandEventType.Grip)
                            {
                                lastHandGripPosition[userID] = new Coordinate2D(hand.X, hand.Y);                              
                            }
                        }

                        var lastHandEvent = lastHandEvents.ContainsKey(userID)
                                                ? lastHandEvents[userID]
                                                : InteractionHandEventType.None;

                        dump.AppendLine();
                        dump.AppendLine("    HandType: " + hand.HandType);
                        dump.AppendLine("    HandEventType: " + hand.HandEventType);
                        dump.AppendLine("    LastHandEventType: " + lastHandEvent);
                        dump.AppendLine("    IsActive: " + hand.IsActive);
                        dump.AppendLine("    IsPrimaryForUser: " + hand.IsPrimaryForUser);
                        dump.AppendLine("    IsInteractive: " + hand.IsInteractive);
                        dump.AppendLine("    PressExtent: " + hand.PressExtent.ToString("N3"));
                        dump.AppendLine("    IsPressed: " + hand.IsPressed);
                        dump.AppendLine("    IsTracked: " + hand.IsTracked);
                        dump.AppendLine("    X: " + hand.X.ToString("N3"));
                        dump.AppendLine("    Y: " + hand.Y.ToString("N3"));
                        dump.AppendLine("    RawX: " + hand.RawX.ToString("N3"));
                        dump.AppendLine("    RawY: " + hand.RawY.ToString("N3"));
                        dump.AppendLine("    RawZ: " + hand.RawZ.ToString("N3"));

                        if (lastHandEvent == InteractionHandEventType.Grip && lastHandGripPosition.ContainsKey(userID))
                        {
                            if (hand.HandType == InteractionHandType.Left)
                            {
                                newLeftHandDragVector = new Coordinate2D(hand.X - _leftHandGripPosition[userID].X, hand.Y - _leftHandGripPosition[userID].Y);
                            }

                            if (hand.HandType == InteractionHandType.Right)
                            {
                                newRightHandDragVector = new Coordinate2D(hand.X - _rightHandGripPosition[userID].X, hand.Y - _rightHandGripPosition[userID].Y);
                            }

                            lastHandGripPosition[userID] = new Coordinate2D(hand.X, hand.Y);
                        }
                    }

                   
                    bool zoom = false;

                    if ((newLeftHandDragVector.X > 0 || newLeftHandDragVector.Y > 0) && (newRightHandDragVector.X > 0 || newRightHandDragVector.Y > 0))
                    {
                        zoom = true;
                        dragVector = new Coordinate2D(newLeftHandDragVector.X + newRightHandDragVector.X, newLeftHandDragVector.Y + newRightHandDragVector.Y);
                    }
                    else if (newRightHandDragVector.X > 0 || newRightHandDragVector.Y > 0)
                    {
                        dragVector = newRightHandDragVector;
                    }
                    else if (newLeftHandDragVector.X > 0 || newLeftHandDragVector.Y > 0)
                    {
                        dragVector = newLeftHandDragVector;
                    }

                    TranslateCamera(dragVector, zoom); // somehow buffer the zooming
                }

                dump.AppendLine("\n\nGrips:");
                if (_leftHandGripPosition.ContainsKey(userID))
                {
                    dump.AppendLine("    LastLeftGripX: " + _leftHandGripPosition[userID].X.ToString());
                    dump.AppendLine("    LastleftGripY: " + _leftHandGripPosition[userID].Y.ToString());
                }
                if (_rightHandGripPosition.ContainsKey(userID))
                {
                    dump.AppendLine("    LastRightGripX: " + _rightHandGripPosition[userID].X.ToString());
                    dump.AppendLine("    LastRightGripY: " + _rightHandGripPosition[userID].Y.ToString());
                }
                dump.AppendLine("    DragVector: " + dragVector.X.ToString() + ", " + dragVector.Y.ToString());

                if (debug)
                {
                    debugWindow.SetText(dump.ToString());

                    if (!hasUser)
                        debugWindow.SetText("No user detected.");
                }
            }
        }

        private void TranslateCamera(Coordinate2D delta, bool zoom)
        {
            if (zoom)
            {
                _helix.CameraController.AddZoomForce(delta.X + delta.Y);
                _sharp.CameraController.AddZoomForce(delta.X + delta.Y);
            }
            else
            {
                _helix.CameraController.AddRotateForce(delta.X * 30, delta.Y * 30);
                _sharp.CameraController.AddRotateForce(delta.X * 30, delta.Y * 30);
            }
        }
    }
}
