﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

using HelixToolkit.Wpf;
using Microsoft.Kinect;
using Microsoft.Kinect.Toolkit;
using Microsoft.Kinect.Toolkit.Controls;
using Microsoft.Kinect.Toolkit.Interaction;

namespace KinectClouds.KinectFramework
{
    class TrackingProvider
    {
        private KinectSensor _sensor;  //The Kinect Sensor the application will use
        private System.Windows.Controls.Control _viewport;
        private KinectRegion _kinectRegion;

        private InteractionStream _interactionStream;
        private UserInfoTranslator _userInfoTranslator;

        private Skeleton[] _skeletons; //the skeletons 
        private UserInfo[] _userInfos; //the information about the interactive users

        private bool _tracking;

        public TrackingProvider(KinectSensor kinect, System.Windows.Controls.Control viewport, KinectRegion kinectRegion)
        {
            _sensor = kinect;
            _tracking = false;
            _viewport = viewport;
            _kinectRegion = kinectRegion;
            _userInfoTranslator = new UserInfoTranslator(_viewport, _kinectRegion);

            _skeletons = new Skeleton[_sensor.SkeletonStream.FrameSkeletonArrayLength]; //!!!
            _userInfos = new UserInfo[InteractionFrame.UserInfoArrayLength];

            _interactionStream = new InteractionStream(_sensor, new InteractionClient()); // !!!
            _interactionStream.InteractionFrameReady += InteractionStreamOnInteractionFrameReady;

            _sensor.DepthFrameReady += SensorOnDepthFrameReady; 
            _sensor.SkeletonFrameReady += SensorOnSkeletonFrameReady;

            _sensor.Start();
        }

        public void StartTracking()
        {
            _tracking = true;
        }

        public void StopTracking()
        {
            _tracking = false;
        }

        public void SetViewport(System.Windows.Controls.Control viewport)
        {
            _userInfoTranslator.SetViewport(viewport);
        }

        private void SensorOnSkeletonFrameReady(object sender, SkeletonFrameReadyEventArgs skeletonFrameReadyEventArgs)
        {
            using (SkeletonFrame skeletonFrame = skeletonFrameReadyEventArgs.OpenSkeletonFrame())
            {
                if (skeletonFrame == null)
                    return;

                try
                {
                    skeletonFrame.CopySkeletonDataTo(_skeletons);
                    var accelerometerReading = _sensor.AccelerometerGetCurrentReading();
                    _interactionStream.ProcessSkeleton(_skeletons, accelerometerReading, skeletonFrame.Timestamp);
                }
                catch (InvalidOperationException)
                {
                    // SkeletonFrame functions may throw when the sensor gets
                    // into a bad state.  Ignore the frame in that case.
                }
            }
        }

        private void SensorOnDepthFrameReady(object sender, DepthImageFrameReadyEventArgs depthImageFrameReadyEventArgs)
        {
            using (DepthImageFrame depthFrame = depthImageFrameReadyEventArgs.OpenDepthImageFrame())
            {
                if (depthFrame == null)
                    return;

                try
                {
                    _interactionStream.ProcessDepth(depthFrame.GetRawPixelData(), depthFrame.Timestamp);
                }
                catch (InvalidOperationException)
                {
                    // DepthFrame functions may throw when the sensor gets
                    // into a bad state.  Ignore the frame in that case.
                }
            }
        }

        private void InteractionStreamOnInteractionFrameReady(object sender, InteractionFrameReadyEventArgs args)
        {
            using (var iaf = args.OpenInteractionFrame()) //dispose as soon as possible
            {
                if (iaf == null)
                    return;

                iaf.CopyInteractionDataTo(_userInfos);
            }

            if (_tracking)
                _userInfoTranslator.ProcessUserInfo(_userInfos);
        }
    }
}
