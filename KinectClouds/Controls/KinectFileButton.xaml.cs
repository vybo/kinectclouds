﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using System.IO;

namespace KinectClouds.Controls
{
    /// <summary>
    /// Interaction logic for KinectFileButton.xaml
    /// </summary>
    public partial class KinectFileButton : UserControl
    {
        // tady nekde data o souboru, ktere se automaticky setnou
        public KinectFileButton()
        {
            InitializeComponent();
        }

        public string Label
        {
            get
            {
                return kbtnFile.Label.ToString();
            }
            set
            {
                kbtnFile.Label = value;
            }
        }

        public Brush LabelBackground
        {
            get
            {
                return kbtnFile.LabelBackground;
            }
            set
            {
                kbtnFile.LabelBackground = value;
            }
        }

        private bool _isParentFolder;
        public bool IsParentFolder
        { 
            get
            {
                return _isParentFolder;
            }
            set
            {
                _isParentFolder = value;
                btnMetadata.Text = "Folder\n\n";
            }
        }

        private FileInfo _fileInfo;
        public FileInfo FileInfo
        {
            get
            {
                return _fileInfo;
            }
            set
            {
                _fileInfo = value;
                kbtnFile.Label = value.Name;
                string metadata = "";
                metadata += "Size: " + ((value.Length / 1024) / 1024).ToString() + " MB\n";
                metadata += "Date: " + value.LastWriteTime.Day + "." + value.LastWriteTime.Month + "." + value.LastWriteTime.Year + "\n";
                metadata += value.LastWriteTime.Hour + ":" + value.LastWriteTime.Minute;
                btnMetadata.Text = metadata;
            }
        }

        private DirectoryInfo _directoryInfo;
        public DirectoryInfo DirectoryInfo
        {
            get
            {
                return _directoryInfo;
            }
            set
            {
                _directoryInfo = value;
                kbtnFile.Label = value.Name;
                btnMetadata.Text = "Folder\n\n";
            }
        }

        public event RoutedEventHandler Click;

        private void kbtnFile_Click(object sender, RoutedEventArgs e)
        {
            if (Click != null) Click(this, e);     
        }

    }
}
