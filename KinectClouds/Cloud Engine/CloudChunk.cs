﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Media3D;
using HelixToolkit.Wpf;
using System.Windows.Media;
using System.Collections;

namespace KinectClouds.Cloud_Engine
{
    class CloudChunk : IEnumerable 
    {
        private List<CloudPoint> _pointsInChunk = new List<CloudPoint>();
        private List<Point3D> _boundaries = new List<Point3D>();

        public CloudChunk(Color color)
        {
            Color = color;
        }

        public CloudChunk()
        {
            Color = Colors.Blue;
        }

        public Color Color { get; set; }

        public void AddPoint(CloudPoint pointToAdd)
        {
            _pointsInChunk.Add(pointToAdd);
        }

        public List<CloudPoint> Points
        {
            get
            {
                return _pointsInChunk;
            }
            set
            {
                _pointsInChunk = value;
            }
        }

        public List<Point3D> Boundaries
        {
            get
            {
                return _boundaries;
            }
            set
            {
                _boundaries = value;
            }
        }

        public int Size
        {
            get
            {
                return _pointsInChunk.Count;
            }
        }

        public IEnumerator GetEnumerator()
        {
            return (IEnumerator)_pointsInChunk.GetEnumerator();
        }
    }
}
