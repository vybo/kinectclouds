﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Media3D;
using HelixToolkit.Wpf;
using System.Windows.Media;
using System.Collections;

namespace KinectClouds.Cloud_Engine
{
    class Cloud : IEnumerable
    {
        private List<CloudChunk> _cloudChunks = new List<CloudChunk>();
        private List<Point3D> _boundaries = new List<Point3D>();
        private List<Point3D> _octrees = new List<Point3D>();

        public Cloud()
        {

        }

        public void AddChunk(CloudChunk chunkToAdd)
        {
            _cloudChunks.Add(chunkToAdd);
        }

        public int Size
        {
            get
            {
                return _cloudChunks.Count;
            }
        }

        // Boundaries for simplification, for viewing and debugging purposes
        public List<Point3D> Boundaries
        {
            get
            {
                return _boundaries;
            }
            set
            {
                _boundaries = value;
            }
        }

        public List<Point3D> Octrees
        {
            get
            {
                return _octrees;
            }
            set
            {
                _octrees = value;
            }
        }

        public List<CloudChunk> Chunks
        {
            get
            {
                return _cloudChunks;
            }
            set
            {
                _cloudChunks = value;
            }
        }

        public CloudChunk GetChunkByIndex(int index)
        {
            return _cloudChunks.ElementAt(index);
        }

        // We need to have this class IEnumerable, so we can use foreach cycle on it. Stuff below is used for this.
        public IEnumerator GetEnumerator()
        {
            return (IEnumerator)_cloudChunks.GetEnumerator();
        }
    }
}
