﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Media3D;
using HelixToolkit.Wpf;
using System.Windows.Media;
using System.Windows.Media.Animation;

namespace KinectClouds.Cloud_Engine
{
    class CloudPoint
    {
        public CloudPoint(Point3D position, double reflectivity)
        {
            Position = position;
            Reflectivity = reflectivity;
        }

        public Point3D Position { get; set; }

        public double Reflectivity { get; set; }
     }
}
