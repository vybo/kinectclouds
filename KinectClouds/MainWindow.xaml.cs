﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Media.Media3D;
using System.Windows.Threading;
using System.Windows.Media.Animation;

using KinectClouds.Cloud_Engine;
using KinectClouds.Cloud_Handling;
//using KinectClouds.Controls;
using KinectClouds.KinectFramework;

using HelixToolkit.Wpf;
using HelixToolkit.Wpf.SharpDX;
using HelixToolkit.Wpf.SharpDX.Core;
using Microsoft.Kinect;
using Microsoft.Kinect.Toolkit;
using Microsoft.Kinect.Toolkit.Interaction;
using System.ComponentModel;
using Microsoft.Kinect.Toolkit.Controls;

namespace KinectClouds
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    /// 

    public partial class MainWindow : Window
    {
        private KinectClouds.Settings.Settings _settings;
        private Cloud _viewedCloud = new Cloud();
        private KinectSensorChooser _sensorChooser;
        private KinectSensor _kinect;
        private TrackingProvider _trackingProvider;
        bool isGripinInteraction = false;
        DispatcherTimer notificationTicker = new DispatcherTimer();
        private bool fastRenderer = false;

        private string _file = "None";
        KinectClouds.Views.MainMenu menu = new KinectClouds.Views.MainMenu();

        public MainWindow()
        {
            InitializeComponent();

            DataContext = this;

            Loaded += OnLoaded;

            Logger.count = 0;
            Logger.count2 = 0;
        }

        private void OnLoaded(object sender, RoutedEventArgs routedEventArgs)
        {
            mainView2.ShowFrameRate = true;
            mainView2.ShowCoordinateSystem = true;
            mainView2.ShowCameraInfo = true;

            _settings = KinectClouds.Settings.Settings.Load();
            notificationTicker.Tick += notificationTicker_Tick;
            notificationTicker.Interval = new TimeSpan(0, 0, 5);
            notificationTicker.Start();
            
            if (_settings.userSitting)
            {
                AddNotificaiton("User tracking set to sitting position.");
            }
            else
            {
                AddNotificaiton("User tracking set to standing position.");
            }
            
            UpdateViewer();

            if (DesignerProperties.GetIsInDesignMode(this))
                return;

            this._sensorChooser = new KinectSensorChooser();
            this._sensorChooser.KinectChanged += SensorChooserOnKinectChanged;
            this.sensorChooserUi.KinectSensorChooser = this._sensorChooser;
            this._sensorChooser.Start();

            KinectRegion.AddQueryInteractionStatusHandler(dragger, OnHandEventQuery);

            if (_settings.positionSelectionAtStart)
                Navigate(NavigationOptions.POSITIONSELECTION);
        }

        private void SensorChooserOnKinectChanged(object sender, KinectChangedEventArgs args)
        {
            bool error = false;
            if (args.OldSensor != null)
            {
                try
                {
                    args.OldSensor.DepthStream.Range = DepthRange.Default;
                    args.OldSensor.SkeletonStream.EnableTrackingInNearRange = false;
                    args.OldSensor.DepthStream.Disable();
                    args.OldSensor.SkeletonStream.Disable();
                }
                catch (InvalidOperationException)
                {
                    error = true;
                }
            }
   
            if (args.NewSensor != null)
            {
                try
                {
                    args.NewSensor.DepthStream.Enable(DepthImageFormat.Resolution640x480Fps30);
                    args.NewSensor.SkeletonStream.Enable();
   
                    try
                    {
//                       args.NewSensor.DepthStream.Range = DepthRange.Near;                    // Nemame Kinect for Windows
//                       args.NewSensor.SkeletonStream.EnableTrackingInNearRange = true;
                        if (_settings.userSitting)
                        {
                            args.NewSensor.SkeletonStream.TrackingMode = SkeletonTrackingMode.Seated;
                        }
                        else
                        {
                            args.NewSensor.SkeletonStream.TrackingMode = SkeletonTrackingMode.Default;
                        }
                    }
                    catch (InvalidOperationException)
                    {
//                       args.NewSensor.DepthStream.Range = DepthRange.Default;
//                       args.NewSensor.SkeletonStream.EnableTrackingInNearRange = false;
                        error = true;
                    }
                }
                catch (InvalidOperationException)
                {
                    error = true;
                }
            }

            if (!error)
            {
                _kinect = args.NewSensor;
                kinectRegion.KinectSensor = _kinect;
                
                if (args.NewSensor != null)
                {
                    _trackingProvider = new TrackingProvider(_kinect, mainView, kinectRegion);
                }
                else
                {
                    _trackingProvider = null;
                }
            } 
        }   
        
        public void UpdateViewer()
        {
            if (fastRenderer)
            {
                this.mainView.Visibility = Visibility.Hidden;
                this.mainView2.Visibility = Visibility.Visible;
                menuItemRendererHelix.IsChecked = false;
                menuItemRendererSharp.IsChecked = true;
            }
            else
            {
                this.mainView.Visibility = Visibility.Visible;
                this.mainView2.Visibility = Visibility.Hidden;
                menuItemRendererHelix.IsChecked = true;
                menuItemRendererSharp.IsChecked = false;
            }

            if (_viewedCloud.Size > 0)
            {
                for (int i = 1; i < mainView.Children.Count; i++)
                    mainView.Children.RemoveAt(i);

                if (fastRenderer)
                {
                    PointGeometry3D points = new PointGeometry3D();
                    Color4Collection col = new Color4Collection();
                    Vector3Collection ptPos = new Vector3Collection();
                    IntCollection ptIdx = new IntCollection();

                    foreach (CloudChunk processingChunk in _viewedCloud)
                    {
                        /*
                        PointGeometry3D points = new PointGeometry3D();
                        Color4Collection col = new Color4Collection();
                        Vector3Collection ptPos = new Vector3Collection();
                        IntCollection ptIdx = new IntCollection();*/
                       
                        /*
                        for (int y = 0; y < height; y++)
                        {
                            for (int x = 0; x < width; x++)
                            {
                                if (depth[y * width + x] < 1000 && depth[y * width + x] > 0)
                                {
                                    ptIdx.Add(ptPos.Count);
                                    ptPos.Add(new Vector3(x, height - y, (-depth[y * width + x] / 3.0f) + 800));
                                    col.Add(rnd.NextColor().ToColor4());
                                }
                            }
                        }*/

                        foreach (CloudPoint processingPoint in processingChunk.Points)
                        {
                            ptIdx.Add(ptPos.Count);
                            ptPos.Add(new SharpDX.Vector3((float)processingPoint.Position.X, (float)processingPoint.Position.Z, (float)processingPoint.Position.Y));
                            col.Add(SharpDX.Color.Blue.ToColor4());
                        }
                    }

                    points.Positions = ptPos;
                    points.Indices = ptIdx;
                    points.Colors = col;
                    allpoints.Geometry = points;
                    allpoints.Color = SharpDX.Color.Aqua;
                    mainView2.InvalidateVisual();
                }                
                else
                {
                    foreach (CloudChunk processingChunk in _viewedCloud)
                    {
                        PointsVisual3D visualChunk = new PointsVisual3D();
                        visualChunk.Size = 1;
                        visualChunk.Color = processingChunk.Color;

                        foreach (CloudPoint processingPoint in processingChunk.Points)
                        {
                            visualChunk.Points.Add(processingPoint.Position);
                        }

                        mainView.Children.Add(visualChunk);

                        LinesVisual3D boundaryCube1 = new LinesVisual3D();
                        PointsVisual3D boundaryPoints = new PointsVisual3D();
                        boundaryPoints.Size = 15;
                        boundaryPoints.Color = Colors.Orange;

                        foreach (Point3D boundary in _viewedCloud.Boundaries)
                        {
                            boundaryCube1.Points.Add(boundary);
                            boundaryPoints.Points.Add(boundary);
                        }

                        boundaryCube1.Thickness = 3;
                        mainView.Children.Add(boundaryCube1);
                        mainView.Children.Add(boundaryPoints);

                        mainView.InvalidateVisual();
                    }
                }
            }

            CloudManipulator man = new CloudManipulator();
            AddNotificaiton("Now showing cloud with " + man.countPoints(_viewedCloud).ToString() + " points.");
        }

        public HelixViewport3D GetMainView()
        {
            return mainView;
        }

        private void menuItemExit_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void menuItemGenerate_Click(object sender, RoutedEventArgs e)
        {
            CloseFile();
            CloudGen generator = new CloudGen();
            _viewedCloud = generator.GenerateCloud(50000, 30, 2000, -2000);
            UpdateViewer();
        }

        private void kbtnTracking_Click(object sender, RoutedEventArgs e)
        {
            if (tbTracking.Text == "Track")
            {
                //tbTracking.Text = "Lock";
                StartTracking();

            }
            else if (tbTracking.Text == "Lock")
            {
                //tbTracking.Text = "Track";
                StopTracking();
            }
        }

        private void kbtnMenu_Click(object sender, RoutedEventArgs e)
        {
            Navigate(NavigationOptions.MENU);
        }

        public enum NavigationOptions { VIEWPORT, MENU, FILE, SCAN, SETTINGS, POSITIONSELECTION, ABOUT, EXIT };

        public void Navigate(NavigationOptions destination)
        {
            Navigate(destination, null);
        }

        public void Navigate(NavigationOptions destination, string uri)
        {
            Control view;
            if (fastRenderer)
            {
                view = mainView2;
            }
            else
            {
                view = mainView;
            }

            switch (destination)
            {
                case NavigationOptions.VIEWPORT:
                                        {
                                            viewPresenter.Content = null;
                                            viewPresenter.Visibility = Visibility.Hidden;
                                            kbtnMenu.Visibility = Visibility.Visible;
                                            kbtnTracking.Visibility = Visibility.Visible;
                                            view.Visibility = Visibility.Visible;
                                            ReloadSettings();
                                            break;
                                        }
                case NavigationOptions.MENU:
                                        {
                                            viewPresenter.Visibility = Visibility.Visible;
                                            viewPresenter.Content = menu;
                                            kbtnTracking.Visibility = Visibility.Hidden;
                                            kbtnMenu.Visibility = Visibility.Hidden;
                                            view.Visibility = Visibility.Hidden;
                                            StopTracking();
                                            tbTracking.Text = "Track";
                                            break;
                                        }
                case NavigationOptions.FILE:
                                        {
                                            KinectClouds.Views.KinectOpenFileDialog openFileDialog = new KinectClouds.Views.KinectOpenFileDialog();
                                            viewPresenter.Content = openFileDialog;
                                            break;
                                        }
                case NavigationOptions.SCAN:
                                        {
                                            AddNotificaiton("Scanning with kinect not yet implemented.");
                                            break;
                                        }
                case NavigationOptions.SETTINGS:
                                        {
                                            KinectClouds.Views.Settings settings = new KinectClouds.Views.Settings(_settings);
                                            viewPresenter.Content = settings;
                                            break;
                                        }
                case NavigationOptions.POSITIONSELECTION:
                                        {
                                            KinectClouds.Views.PositionSelection selection = new KinectClouds.Views.PositionSelection(_settings);
                                            viewPresenter.Content = selection;
                                            viewPresenter.Visibility = Visibility.Visible;
                                            kbtnTracking.Visibility = Visibility.Hidden;
                                            kbtnMenu.Visibility = Visibility.Hidden;
                                            view.Visibility = Visibility.Hidden;
                                            StopTracking();
                                            break;
                                        }
                case NavigationOptions.ABOUT:
                                        {
                                            AddNotificaiton("About screen is not yet implemented.");
                                            break;
                                        }
                case NavigationOptions.EXIT:
                                        {
                                            _settings.Save();
                                            this.Close();
                                            break;
                                        }
                default:
                    break;

            }
        }

        public void AddNotificaiton(string text)
        {
            TextBlock lost = AddNotificationEditable(text);
        }

        public TextBlock AddNotificationEditable(string text)
        {
            KinectTileButton newNotif = new KinectTileButton();
            TextBlock intext = new TextBlock();
            Viewbox box = new Viewbox();
            newNotif.Margin = new Thickness(0, 5, 0, 0);
            intext.Foreground = Brushes.White;
            intext.Text = text;
            box.Child = intext;
            box.Margin = new Thickness(5, 0, 5, 0);
            newNotif.HorizontalAlignment = System.Windows.HorizontalAlignment.Right;
            newNotif.Width = 400;
            newNotif.Height = 100;
            newNotif.Content = box;
            newNotif.Click += newNotif_Click;

            if (NotificationPanel.Children.Count > 5)
            {
                NotificationPanel.Children.RemoveAt(0);
                NotificationPanel.Children.Add(newNotif);
            }
            else
            {
                NotificationPanel.Children.Add(newNotif);
            }

            return intext;
        }

        private void ReloadSettings()
        {
            if (_settings.userSitting)
            {
                AddNotificaiton("User tracking set to sitting position.");
            }
            else
            {
                AddNotificaiton("User tracking set to standing position.");
            }

            _sensorChooser.Stop();
            _sensorChooser.Start();
        }

        void newNotif_Click(object sender, RoutedEventArgs e)
        {
            KinectTileButton senderButton = (KinectTileButton)sender;

            NotificationPanel.Children.Remove(senderButton);
        }

        private void StartTracking()
        {
            if (_kinect != null)
            {
                dragger.Visibility = Visibility.Visible;
                 tbTracking.Text = "Lock";
                _trackingProvider.StartTracking();
            }
            else
            {
                AddNotificaiton("Sensor error.");
                tbTracking.Text = "Track";
            }
        }

        private void StopTracking()
        {
            if (_kinect != null)
            {
                dragger.Visibility = Visibility.Hidden;
                _trackingProvider.StopTracking();
                tbTracking.Text = "Track";
            }
        }

        private void OnHandEventQuery(object sender, QueryInteractionStatusEventArgs handPointerEventArgs)
        {

            //If a grip detected change the cursor image to grip
            if (handPointerEventArgs.HandPointer.HandEventType == HandEventType.Grip)
            {
                isGripinInteraction = true;
                handPointerEventArgs.IsInGripInteraction = true;
            }

           //If Grip Release detected change the cursor image to open
            else if (handPointerEventArgs.HandPointer.HandEventType == HandEventType.GripRelease)
            {
                isGripinInteraction = false;
                handPointerEventArgs.IsInGripInteraction = false;
            }

            //If no change in state do not change the cursor
            else if (handPointerEventArgs.HandPointer.HandEventType == HandEventType.None)
            {
                handPointerEventArgs.IsInGripInteraction = isGripinInteraction;
            }

            handPointerEventArgs.Handled = true;
        }

        private void notificationTicker_Tick(object sender, EventArgs e)
        {
            if (NotificationPanel.Children.Count > 0)
                NotificationPanel.Children.RemoveAt(0);
        }

        private void menuItemTestNotif_Click(object sender, RoutedEventArgs e)
        {
            AddNotificaiton("Testing notifications....");
        }

        private void Window_Closing(object sender, CancelEventArgs e)
        {
            AddNotificaiton("Saving necessary data and closing...");
            StopTracking();
            _sensorChooser.Stop();
            _settings.Save();
        }

        private void menuItemOpen_Click(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog openFileDialog = new Microsoft.Win32.OpenFileDialog();
            openFileDialog.DefaultExt = ".txt";
            openFileDialog.Filter = "Text documents with Point Cloud data (.txt)|*.txt";
            bool? opened = openFileDialog.ShowDialog();

            if (opened == true)
            {
                _file = openFileDialog.FileName;
                OpenFile(_file);
            }
        }

        public async void OpenFile(string fileName)
        {
            CloudLoader loader = new CloudLoader();

            /*
            _viewedCloud = loader.Load(fileName);
            UpdateViewer();
            AddNotificaiton("Opened file");
             * */
            
            try
            {
                //_viewedCloud = loader.Load(fileName, true);
                AddNotificaiton("Opening file " + fileName);

                _viewedCloud = await loader.LoadAsync(fileName, true, mainView);

                UpdateViewer();
                AddNotificaiton("Opened file " + fileName);
                menu.UpperText = "Current file: " + fileName;
            }
            catch
            {
                AddNotificaiton("There was an error while processing your file!");
            }
        }

        public void CloseFile()
        {
            _file = null;
            menu.UpperText = "Current file: None";
            _viewedCloud = new Cloud();
            
            for (int i = 1; i < mainView.Children.Count; i++)
                mainView.Children.RemoveAt(i);

            PointGeometry3D empty = new PointGeometry3D();
            allpoints.Geometry = empty;

            UpdateViewer();
            AddNotificaiton("Closed file");
        }

        private void menuItemClose_Click(object sender, RoutedEventArgs e)
        {
            CloseFile();
        }

        private void menuItemRendererHelix_Click(object sender, RoutedEventArgs e)
        {
            fastRenderer = false;
            UpdateViewer();
        }

        private void menuItemRendererSharp_Click(object sender, RoutedEventArgs e)
        {
            fastRenderer = true;
            UpdateViewer();
        }

        private void menuItemSimplify30_Click(object sender, RoutedEventArgs e)
        {
            CloudManipulator manipulator = new CloudManipulator();

            try
            {
                _viewedCloud = manipulator.SimplifyCloud(_viewedCloud, 30000);
                AddNotificaiton("Completed simplification with " + Logger.count.ToString() + " errors.");
                Logger.Log("Completed simplification with " + Logger.count.ToString() + " errors.");
                Logger.count = 0;
                Logger.count2 = 0;
            }
            catch (System.IndexOutOfRangeException ex)
            {
                AddNotificaiton("Error: " + ex.Message);
            }

            mainView.Children.Clear();
            UpdateViewer();
        }

        private void menuItemSimplifyStep_Click(object sender, RoutedEventArgs e)
        {
            CloudManipulator manipulator = new CloudManipulator();

            try
            {
                _viewedCloud = manipulator.SimplifyCloudStep(_viewedCloud);
            }
            catch (System.IndexOutOfRangeException ex)
            {
                AddNotificaiton("Error: " + ex.Message);
            }

            mainView.Children.Clear();
            UpdateViewer();
        }

        private void menuItemSimplifyBy30_Click(object sender, RoutedEventArgs e)
        {
            CloudManipulator manipulator = new CloudManipulator();

            try
            {
                _viewedCloud = manipulator.SimplifyCloudBy(_viewedCloud, 30000);
            }
            catch (System.IndexOutOfRangeException ex)
            {
                AddNotificaiton("Error: " + ex.Message);
            }

            mainView.Children.Clear();
            UpdateViewer();
        }
    }
}