﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using System.IO;
using KinectClouds.Controls;

namespace KinectClouds.Views
{
    /// <summary>
    /// Interaction logic for KinectOpenFileDialog.xaml
    /// </summary>
    public partial class KinectOpenFileDialog : UserControl
    {
        DirectoryInfo currentDirectoryInfo;

        public KinectOpenFileDialog()
        {
            InitializeComponent();
            currentDirectoryInfo = new DirectoryInfo(Directory.GetCurrentDirectory());
            LoadContent(Directory.GetCurrentDirectory());
        }

        private void LoadContent(string directoryPath)
        {
            FileItems.Items.Clear();
            currentDirectoryInfo = new DirectoryInfo(directoryPath);
            tbCurrentFolder.Text = "Current folder: " + currentDirectoryInfo.FullName;
            // Get only text files for now
            List<FileInfo> filesInDirectory = currentDirectoryInfo.GetFiles("*.txt").ToList<FileInfo>();
            List<DirectoryInfo> foldersInDirectory = currentDirectoryInfo.GetDirectories().ToList<DirectoryInfo>();
            DirectoryInfo parentDirectoryInfo = Directory.GetParent(currentDirectoryInfo.FullName);

            KinectFileButton parentDirButton = new KinectFileButton();
            parentDirButton.Height = 220;
            parentDirButton.Width = 220;
            parentDirButton.LabelBackground = Brushes.DarkBlue;
            parentDirButton.Foreground = Brushes.White;
            parentDirButton.DirectoryInfo = parentDirectoryInfo;
            parentDirButton.Label = "..";
            parentDirButton.Click += FileButton_Click;

            FileItems.Items.Add(parentDirButton);

            foreach (DirectoryInfo directory in foldersInDirectory)
            {
                KinectFileButton newButton = new KinectFileButton();
                newButton.Height = 220;
                newButton.Width = 220;
                newButton.Label = "..";
                newButton.LabelBackground = Brushes.DarkBlue;
                newButton.Foreground = Brushes.White;
                newButton.DirectoryInfo = directory;
                newButton.Click += FileButton_Click;

                FileItems.Items.Add(newButton);
            }

            foreach (FileInfo file in filesInDirectory)
            {
                KinectFileButton newButton = new KinectFileButton();
                newButton.Height = 220;
                newButton.Width = 220;
                newButton.Label = "..";
                newButton.LabelBackground = Brushes.DarkBlue;
                newButton.Foreground = Brushes.White;
                newButton.FileInfo = file;
                newButton.Click += FileButton_Click;

                FileItems.Items.Add(newButton);
            }
        }

        private void FileButton_Click(object sender, RoutedEventArgs e)
        {
            KinectFileButton senderButton = (KinectFileButton)sender;

            if (senderButton.DirectoryInfo != null)
            {
                LoadContent(senderButton.DirectoryInfo.FullName);
            }
            else if (senderButton.FileInfo != null)
            {
                ((MainWindow)Application.Current.MainWindow).OpenFile(senderButton.FileInfo.FullName);
                ((MainWindow)Application.Current.MainWindow).Navigate(MainWindow.NavigationOptions.MENU);
            }
            else
            {
                ((MainWindow)Application.Current.MainWindow).AddNotificaiton("Something went wrong.\nNo directory or file associated with this button.");
            }
        }

        private void kbtnBack_Click(object sender, RoutedEventArgs e)
        {
            ((MainWindow)Application.Current.MainWindow).Navigate(MainWindow.NavigationOptions.MENU);
        }
    }
}
