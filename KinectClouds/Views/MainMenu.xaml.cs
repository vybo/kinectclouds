﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace KinectClouds.Views
{
    /// <summary>
    /// Interaction logic for MainMenu.xaml
    /// </summary>
    public partial class MainMenu : UserControl
    {
        public string UpperText
        {
            get
            {
                return tbCurrentFile.Text; ;
            }
            set
            {
                tbCurrentFile.Text = value;
            }
        }

        public MainMenu()
        {
            InitializeComponent();
        }

        private void kbtnScan_Click(object sender, RoutedEventArgs e)
        {
            ((MainWindow)Application.Current.MainWindow).Navigate(MainWindow.NavigationOptions.SCAN);
        }

        private void kbtnFile_Click(object sender, RoutedEventArgs e)
        {
            ((MainWindow)Application.Current.MainWindow).Navigate(MainWindow.NavigationOptions.FILE);
        }

        private void kbtnViewport_Click(object sender, RoutedEventArgs e)
        {
            ((MainWindow)Application.Current.MainWindow).Navigate(MainWindow.NavigationOptions.VIEWPORT);
        }

        private void kbtnSettings_Click(object sender, RoutedEventArgs e)
        {
            ((MainWindow)Application.Current.MainWindow).Navigate(MainWindow.NavigationOptions.SETTINGS);
        }

        private void kbtnAbout_Click(object sender, RoutedEventArgs e)
        {
            ((MainWindow)Application.Current.MainWindow).Navigate(MainWindow.NavigationOptions.ABOUT);
        }

        private void kbtnExit_Click(object sender, RoutedEventArgs e)
        {
            ((MainWindow)Application.Current.MainWindow).Navigate(MainWindow.NavigationOptions.EXIT);
        }
    }
}
