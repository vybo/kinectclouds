﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace KinectClouds.Views
{
    /// <summary>
    /// Interaction logic for PositionSelection.xaml
    /// </summary>
    public partial class PositionSelection : UserControl
    {
        private KinectClouds.Settings.Settings _settings;
        public PositionSelection(KinectClouds.Settings.Settings settings)
        {
            InitializeComponent();

            _settings = settings;
        }

        private void kbtnSitting_Click(object sender, RoutedEventArgs e)
        {
            _settings.userSitting = true;
            ((MainWindow)Application.Current.MainWindow).Navigate(MainWindow.NavigationOptions.VIEWPORT);
        }

        private void kbtnStanding_Click(object sender, RoutedEventArgs e)
        {
            _settings.userSitting = false;
            ((MainWindow)Application.Current.MainWindow).Navigate(MainWindow.NavigationOptions.VIEWPORT);
        }
    }
}
