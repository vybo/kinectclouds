﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace KinectClouds.Views
{
    /// <summary>
    /// Interaction logic for Settings.xaml
    /// </summary>
    public partial class Settings : UserControl
    {
        private KinectClouds.Settings.Settings _settings;
        public Settings(KinectClouds.Settings.Settings settings)
        {
            InitializeComponent();

            _settings = settings;

            if (_settings.userSitting)
            {
                tbUserPositionStatus.Text = "Sit";
            }
            else
            {
                tbUserPositionStatus.Text = "Stand";
            }
        }

        private void kbtnUserPosition_Click(object sender, RoutedEventArgs e)
        {
            if (tbUserPositionStatus.Text == "Sit")
            {
                tbUserPositionStatus.Text = "Stand";
                _settings.userSitting = false;
            }
            else
            {
                tbUserPositionStatus.Text = "Sit";
                _settings.userSitting = true;
            }
        }

        private void kbtnPositionSelectionAtStart_Click(object sender, RoutedEventArgs e)
        {
            if (tbPositionSelectionAtStartStatus.Text == "Off")
            {
                tbPositionSelectionAtStartStatus.Text = "On";
                _settings.positionSelectionAtStart = true;
            }
            else
            {
                tbPositionSelectionAtStartStatus.Text = "Off";
                _settings.positionSelectionAtStart = false;
            }
        }

        private void kbtnBack_Click(object sender, RoutedEventArgs e)
        {
            ((MainWindow)Application.Current.MainWindow).Navigate(MainWindow.NavigationOptions.MENU);
        }
    }
}
