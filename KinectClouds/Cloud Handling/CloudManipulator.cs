﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using KinectClouds.Cloud_Engine;
using System.Windows.Media.Media3D;

namespace KinectClouds.Cloud_Handling
{
    class CloudManipulator
    {
       
        public CloudManipulator()
        {

        }

        public Cloud SimplifyCloudStep(Cloud cloud)
        {
            FastCloud yolo = new FastCloud();
            yolo.Insert(cloud.Chunks.ElementAt(0).Points);
            yolo.Split();

            Cloud simplifiedCloud = yolo.MakeCloud();
            return simplifiedCloud;
        }

        public Cloud SimplifyCloudBy(Cloud cloud, int step)
        {
            FastCloud yolo = new FastCloud();
            yolo.Insert(cloud.Chunks.ElementAt(0).Points);

            while ((yolo.CountChildren() - step ) > yolo.CountChildren())
            {
                yolo.Split();
            }



            Cloud simplifiedCloud = yolo.MakeCloud();
            return simplifiedCloud;
        }

        public Cloud SimplifyCloud(Cloud cloud, int desiredCloudSize)
        {          
            FastCloud yolo = new FastCloud();
            yolo.Insert(cloud.Chunks.ElementAt(0).Points);

            int count = yolo.CountChildren();
            int pass = 0;
            int diff = count - desiredCloudSize;
            double prevCount = 0;
            while (prevCount > diff)
            {
                System.Diagnostics.Trace.WriteLine("Octree level " + (++pass).ToString());

                yolo.Split();
                prevCount = count;
                count = yolo.CountChildren();
                diff = count - desiredCloudSize;
            }

            //TODO POTREBA DODELAT PODMINKU WHILE!!!!!
            //yolo.Split();
            yolo.Split();

            yolo.SimplifyPoints();
            Cloud simplifiedCloud = yolo.MakeCloud();
            return simplifiedCloud;
        }

        private bool belongsToChunk(CloudPoint point, CloudChunk chunk)
        {
            //Check if belongs by X coords
            if (
                point.Position.X > chunk.Boundaries.ElementAt(0).X &&
                point.Position.X > chunk.Boundaries.ElementAt(1).X &&
                point.Position.X < chunk.Boundaries.ElementAt(2).X &&
                point.Position.X < chunk.Boundaries.ElementAt(3).X &&
                point.Position.X > chunk.Boundaries.ElementAt(4).X &&
                point.Position.X > chunk.Boundaries.ElementAt(5).X &&
                point.Position.X < chunk.Boundaries.ElementAt(6).X &&
                point.Position.X < chunk.Boundaries.ElementAt(7).X
                )
            {
                // Check if belongs by Y coords
                if (
                    point.Position.Y > chunk.Boundaries.ElementAt(0).Y &&
                    point.Position.Y < chunk.Boundaries.ElementAt(1).Y &&
                    point.Position.Y > chunk.Boundaries.ElementAt(2).Y &&
                    point.Position.Y < chunk.Boundaries.ElementAt(3).Y &&
                    point.Position.Y > chunk.Boundaries.ElementAt(4).Y &&
                    point.Position.Y < chunk.Boundaries.ElementAt(5).Y &&
                    point.Position.Y > chunk.Boundaries.ElementAt(6).Y &&
                    point.Position.Y < chunk.Boundaries.ElementAt(7).Y
                    )
                {
                    // Check if belongs by Z coords
                    if (
                        point.Position.Z > chunk.Boundaries.ElementAt(0).Z &&
                        point.Position.Z > chunk.Boundaries.ElementAt(1).Z &&
                        point.Position.Z > chunk.Boundaries.ElementAt(2).Z &&
                        point.Position.Z > chunk.Boundaries.ElementAt(3).Z &&
                        point.Position.Z < chunk.Boundaries.ElementAt(4).Z &&
                        point.Position.Z < chunk.Boundaries.ElementAt(5).Z &&
                        point.Position.Z < chunk.Boundaries.ElementAt(6).Z &&
                        point.Position.Z < chunk.Boundaries.ElementAt(7).Z
                        )
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        public int countPoints(Cloud inCloud)
        {
            int count = 0;

            foreach (CloudChunk chunk in inCloud)
            {
                foreach (CloudPoint point in chunk)
                {
                    count++;
                }
            }

            return count;
        }
    }
}
