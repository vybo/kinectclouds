﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.IO;
using System.Windows.Media;
using System.Windows.Media.Media3D;

using KinectClouds.Cloud_Engine;
using HelixToolkit.Wpf;

namespace KinectClouds.Cloud_Handling
{
    class CloudLoader
    {
        private Cloud _loadedCloud;

        public CloudLoader()
        {
            _loadedCloud = new Cloud();
        }

        public Cloud Load(string fileName)
        {
            return Load(fileName, false);
        }

        public Cloud Load(string fileName, bool moveToZero)
        {
            string line;
            double averageX = 0;
            double averageY = 0;
            double averageZ = 0;

            CloudChunk newChunk = new CloudChunk(Colors.Blue);
            StreamReader reader = new StreamReader(fileName);

            while ((line = reader.ReadLine()) != null)
            {
                string[] values = line.Split(' ');
                double x = double.Parse(values[0], System.Globalization.CultureInfo.InvariantCulture);
                double y = double.Parse(values[1], System.Globalization.CultureInfo.InvariantCulture);
                double z = double.Parse(values[2], System.Globalization.CultureInfo.InvariantCulture);
                double reflectivity = double.Parse(values[3], System.Globalization.CultureInfo.InvariantCulture);

                CloudPoint newPoint = new CloudPoint(new Point3D(x, y, z), reflectivity);
                averageX += x;
                averageY += y;
                averageZ += z;
                
                newChunk.AddPoint(newPoint);
            }

            if (moveToZero)
            {
                // Center of Mass for moving the cloud to the center of the unit system
                Point3D centerOfMass = new Point3D(averageX / newChunk.Points.Count, averageY / newChunk.Points.Count, averageZ / newChunk.Points.Count);

                foreach (CloudPoint point in newChunk)
                {
                    Point3D newPosition = point.Position;
                    newPosition.X -= centerOfMass.X;
                    newPosition.Y -= centerOfMass.Y;
                    newPosition.Z -= centerOfMass.Z;
                    point.Position = newPosition;
                }
            }

            _loadedCloud.AddChunk(newChunk);
            return _loadedCloud;
        }        

        public async Task<Cloud> LoadAsync(string fileName, bool moveToZero, HelixViewport3D view)
        {
            string line;
            double averageX = 0;
            double averageY = 0;
            double averageZ = 0;

            CloudChunk newChunk = new CloudChunk(Colors.Blue);
            StreamReader reader = new StreamReader(fileName);

            while ((line = await reader.ReadLineAsync()) != null)
            {
                string[] values = line.Split(' ');
                double x = double.Parse(values[0], System.Globalization.CultureInfo.InvariantCulture);
                double y = double.Parse(values[1], System.Globalization.CultureInfo.InvariantCulture);
                double z = double.Parse(values[2], System.Globalization.CultureInfo.InvariantCulture);
                double reflectivity = double.Parse(values[3], System.Globalization.CultureInfo.InvariantCulture);

                CloudPoint newPoint = new CloudPoint(new Point3D(x, y, z), reflectivity);
                averageX += x;
                averageY += y;
                averageZ += z;

                newChunk.AddPoint(newPoint);
                //view.Children.Add(Visual)
            }

            if (moveToZero)
            {
                // Center of Mass for moving the cloud to the center of the unit system
                Point3D centerOfMass = new Point3D(averageX / newChunk.Points.Count, averageY / newChunk.Points.Count, averageZ / newChunk.Points.Count);

                foreach (CloudPoint point in newChunk)
                {
                    Point3D newPosition = point.Position;
                    newPosition.X -= centerOfMass.X;
                    newPosition.Y -= centerOfMass.Y;
                    newPosition.Z -= centerOfMass.Z;
                    point.Position = newPosition;
                }
            }

            _loadedCloud.AddChunk(newChunk);
            return _loadedCloud;
        }
    }
}
