﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using KinectClouds.Cloud_Engine;
using System.Windows.Media.Media3D;
using System.Collections;
using System.Windows.Media;

namespace KinectClouds.Cloud_Handling
{
    class FastCloud : IEnumerable
    {
        private List<CloudPoint> _points = new List<CloudPoint>();
        private List<Point3D> _boundaries = new List<Point3D>();

        public Point3D boundaryLeftBottomFront { get; set; }
        public Point3D boundaryRightBottomFront { get; set; }
        public Point3D boundaryLeftBottomBack { get; set; }
        public Point3D boundaryRightBottomBack { get; set; }

        public Point3D boundaryLeftTopFront { get; set; }
        public Point3D boundaryRightTopFront { get; set; }
        public Point3D boundaryLeftTopBack { get; set; }
        public Point3D boundaryRightTopBack { get; set; }

        private FastCloud _parent;

        private FastCloud _child1 = null;
        private FastCloud _child2 = null;
        private FastCloud _child3 = null;
        private FastCloud _child4 = null;
        private FastCloud _child5 = null;
        private FastCloud _child6 = null;
        private FastCloud _child7 = null;
        private FastCloud _child8 = null;

        public void Split()
        {
            if (_child1 == null)
            {
                _child1 = new FastCloud(this);
                _child2 = new FastCloud(this);
                _child3 = new FastCloud(this);
                _child4 = new FastCloud(this);
                _child5 = new FastCloud(this);
                _child6 = new FastCloud(this);
                _child7 = new FastCloud(this);
                _child8 = new FastCloud(this);

                double xSplit = (boundaryRightBottomBack.X - boundaryLeftBottomBack.X) / 2;
                double ySplit = (boundaryLeftBottomFront.Y - boundaryLeftTopFront.Y) / 2;
                double zSplit = (boundaryRightBottomBack.Z - boundaryRightBottomFront.Z) / 2;

                _child1.boundaryLeftBottomFront = boundaryLeftBottomFront; // We don't need unnecessary objects
                _child1.boundaryRightBottomFront = new Point3D(boundaryLeftBottomFront.X + xSplit, boundaryLeftBottomFront.Y, boundaryLeftBottomFront.Z);
                _child1.boundaryLeftBottomBack = new Point3D(boundaryLeftBottomFront.X, boundaryLeftBottomFront.Y, boundaryLeftBottomFront.Z + zSplit);
                _child1.boundaryRightBottomBack = new Point3D(boundaryLeftBottomFront.X + xSplit, boundaryLeftBottomFront.Y, boundaryLeftBottomFront.Z + zSplit);
                _child1.boundaryLeftTopFront = new Point3D(boundaryLeftTopFront.X, boundaryLeftTopFront.Y + ySplit, boundaryLeftTopFront.Z);
                _child1.boundaryRightTopFront = new Point3D(boundaryLeftTopFront.X + xSplit, boundaryLeftTopFront.Y + ySplit, boundaryLeftTopFront.Z);
                _child1.boundaryLeftTopBack = new Point3D(boundaryLeftTopFront.X, boundaryLeftTopFront.Y + ySplit, boundaryLeftTopFront.Z + zSplit);
                _child1.boundaryRightTopBack = new Point3D(boundaryLeftTopFront.X + xSplit, boundaryLeftTopFront.Y + ySplit, boundaryLeftTopFront.Z + zSplit);
                _child1.PutBoundaries();

                _child2.boundaryLeftBottomFront = _child1.boundaryRightBottomFront;
                _child2.boundaryRightBottomFront = boundaryRightBottomFront;
                _child2.boundaryLeftBottomBack = _child1.boundaryRightBottomBack;
                _child2.boundaryRightBottomBack = new Point3D(boundaryRightBottomFront.X, boundaryRightBottomFront.Y, boundaryRightBottomFront.Z + zSplit);
                _child2.boundaryLeftTopFront = _child1.boundaryRightTopFront;
                _child2.boundaryRightTopFront = new Point3D(boundaryRightTopFront.X, boundaryRightTopFront.Y + ySplit, boundaryRightTopFront.Z);
                _child2.boundaryLeftTopBack = _child1.boundaryRightTopBack;
                _child2.boundaryRightTopBack = new Point3D(boundaryRightTopFront.X, boundaryRightTopFront.Y + ySplit, boundaryRightTopFront.Z + zSplit);
                _child2.PutBoundaries();

                _child3.boundaryLeftBottomFront = _child1.boundaryLeftBottomBack;
                _child3.boundaryRightBottomFront = _child1.boundaryRightBottomBack;
                _child3.boundaryLeftBottomBack = boundaryLeftBottomBack;
                _child3.boundaryRightBottomBack = new Point3D(boundaryLeftBottomBack.X + xSplit, boundaryLeftBottomBack.Y, boundaryLeftBottomBack.Z);
                _child3.boundaryLeftTopFront = _child1.boundaryLeftTopBack;
                _child3.boundaryRightTopFront = _child1.boundaryRightTopBack;
                _child3.boundaryLeftTopBack = new Point3D(boundaryLeftTopBack.X, boundaryLeftTopBack.Y + ySplit, boundaryLeftTopBack.Z);
                _child3.boundaryRightTopBack = new Point3D(boundaryLeftTopBack.X + xSplit, boundaryLeftTopBack.Y + ySplit, boundaryLeftTopBack.Z);
                _child3.PutBoundaries();

                _child4.boundaryLeftBottomFront = _child1.boundaryRightBottomBack;
                _child4.boundaryRightBottomFront = _child2.boundaryRightBottomBack;
                _child4.boundaryLeftBottomBack = _child3.boundaryRightBottomBack;
                _child4.boundaryRightBottomBack = boundaryRightBottomFront;
                _child4.boundaryLeftTopFront = _child1.boundaryRightTopBack;
                _child4.boundaryRightTopFront = _child2.boundaryRightTopBack;
                _child4.boundaryLeftTopBack = _child3.boundaryRightTopBack;
                _child4.boundaryRightTopBack = new Point3D(boundaryRightTopBack.X, boundaryRightTopBack.Y + ySplit, boundaryRightTopBack.Z);
                _child4.PutBoundaries();

                _child5.boundaryLeftBottomFront = _child1.boundaryLeftTopFront;
                _child5.boundaryRightBottomFront = _child1.boundaryRightTopFront;
                _child5.boundaryLeftBottomBack = _child1.boundaryLeftTopBack;
                _child5.boundaryRightBottomBack = _child1.boundaryRightTopBack;
                _child5.boundaryLeftTopFront = boundaryLeftTopFront;
                _child5.boundaryRightTopFront = new Point3D(boundaryLeftTopFront.X + xSplit, boundaryLeftTopFront.Y, boundaryLeftTopFront.Z);
                _child5.boundaryLeftTopBack = new Point3D(boundaryLeftTopFront.X, boundaryLeftTopFront.Y, boundaryLeftTopFront.Z + zSplit);
                _child5.boundaryRightTopBack = new Point3D(boundaryLeftTopFront.X + xSplit, boundaryLeftTopFront.Y, boundaryLeftTopFront.Z + zSplit);
                _child5.PutBoundaries();

                _child6.boundaryLeftBottomFront = _child1.boundaryRightTopFront;
                _child6.boundaryRightBottomFront = _child2.boundaryRightTopFront;
                _child6.boundaryLeftBottomBack = _child1.boundaryRightTopBack;
                _child6.boundaryRightBottomBack = _child2.boundaryRightTopBack;
                _child6.boundaryLeftTopFront = _child5.boundaryRightTopFront;
                _child6.boundaryRightTopFront = boundaryRightTopFront;
                _child6.boundaryLeftTopBack = _child5.boundaryRightTopBack;
                _child6.boundaryRightTopBack = new Point3D(boundaryRightTopFront.X, boundaryRightTopFront.Y, boundaryRightTopFront.Z + zSplit);
                _child6.PutBoundaries();

                _child7.boundaryLeftBottomFront = _child1.boundaryLeftTopBack;
                _child7.boundaryRightBottomFront = _child1.boundaryRightTopBack;
                _child7.boundaryLeftBottomBack = _child3.boundaryLeftTopBack;
                _child7.boundaryRightBottomBack = _child3.boundaryRightTopBack;
                _child7.boundaryLeftTopFront = _child5.boundaryLeftTopBack;
                _child7.boundaryRightTopFront = _child5.boundaryRightTopBack;
                _child7.boundaryLeftTopBack = boundaryLeftTopBack;
                _child7.boundaryRightTopBack = new Point3D(boundaryLeftTopBack.X + xSplit, boundaryLeftTopBack.Y, boundaryLeftTopBack.Z);
                _child7.PutBoundaries();

                _child8.boundaryLeftBottomFront = _child1.boundaryRightTopBack;
                _child8.boundaryRightBottomFront = _child2.boundaryRightTopBack;
                _child8.boundaryLeftBottomBack = _child3.boundaryRightTopBack;
                _child8.boundaryRightBottomBack = _child4.boundaryRightTopBack;
                _child8.boundaryLeftTopFront = _child5.boundaryRightTopBack;
                _child8.boundaryRightTopFront = _child6.boundaryRightTopBack;
                _child8.boundaryLeftTopBack = _child7.boundaryRightTopBack;
                _child8.boundaryRightTopBack = boundaryRightTopBack;
                _child8.PutBoundaries();

                foreach (CloudPoint point in _points)
                {
                    if (belongsToChild(point, _child1))
                    {
                        _child1.Points.Add(point);
                    }
                    else if (belongsToChild(point, _child2))
                    {
                        _child2.Points.Add(point);
                    }
                    else if (belongsToChild(point, _child3))
                    {
                        _child3.Points.Add(point);
                    }
                    else if (belongsToChild(point, _child4))
                    {
                        _child4.Points.Add(point);
                    }
                    else if (belongsToChild(point, _child5))
                    {
                        _child5.Points.Add(point);
                    }
                    else if (belongsToChild(point, _child6))
                    {
                        _child6.Points.Add(point);
                    }
                    else if (belongsToChild(point, _child7))
                    {
                        _child7.Points.Add(point);
                    }
                    else if (belongsToChild(point, _child8))
                    {
                        _child8.Points.Add(point);
                    }
                    else
                    {
                        Logger.count++;

                        Logger.Log("====================================================================");
                        Logger.Log("{ " + point.Position.X.ToString() + " , " + point.Position.Y.ToString() + " , " + point.Position.Z.ToString() + "} Point doesnt belong to any child.");
                        Logger.Log("Child 1:");
                        Logger.Log("Xmin: " + _child1.boundaryLeftBottomFront.X.ToString() + " Xmax: " + _child1.boundaryRightBottomFront.X.ToString());
                        Logger.Log("Ymin: " + _child1.boundaryLeftTopFront.Y.ToString() + " Ymax: " + _child1.boundaryLeftBottomFront.Y.ToString());
                        Logger.Log("Zmin: " + _child1.boundaryLeftTopFront.Z.ToString() + " Zmax: " + _child1.boundaryLeftTopBack.Z.ToString());

                        Logger.Log("Child 2:");
                        Logger.Log("Xmin: " + _child2.boundaryLeftBottomFront.X.ToString() + " Xmax: " + _child2.boundaryRightBottomFront.X.ToString());
                        Logger.Log("Ymin: " + _child2.boundaryLeftTopFront.Y.ToString() + " Ymax: " + _child2.boundaryLeftBottomFront.Y.ToString());
                        Logger.Log("Zmin: " + _child2.boundaryLeftTopFront.Z.ToString() + " Zmax: " + _child2.boundaryLeftTopBack.Z.ToString());

                        Logger.Log("Child 3:");
                        Logger.Log("Xmin: " + _child3.boundaryLeftBottomFront.X.ToString() + " Xmax: " + _child3.boundaryRightBottomFront.X.ToString());
                        Logger.Log("Ymin: " + _child3.boundaryLeftTopFront.Y.ToString() + " Ymax: " + _child3.boundaryLeftBottomFront.Y.ToString());
                        Logger.Log("Zmin: " + _child3.boundaryLeftTopFront.Z.ToString() + " Zmax: " + _child3.boundaryLeftTopBack.Z.ToString());

                        Logger.Log("Child 4:");
                        Logger.Log("Xmin: " + _child4.boundaryLeftBottomFront.X.ToString() + " Xmax: " + _child4.boundaryRightBottomFront.X.ToString());
                        Logger.Log("Ymin: " + _child4.boundaryLeftTopFront.Y.ToString() + " Ymax: " + _child4.boundaryLeftBottomFront.Y.ToString());
                        Logger.Log("Zmin: " + _child4.boundaryLeftTopFront.Z.ToString() + " Zmax: " + _child4.boundaryLeftTopBack.Z.ToString());

                        Logger.Log("Child 5:");
                        Logger.Log("Xmin: " + _child5.boundaryLeftBottomFront.X.ToString() + " Xmax: " + _child5.boundaryRightBottomFront.X.ToString());
                        Logger.Log("Ymin: " + _child5.boundaryLeftTopFront.Y.ToString() + " Ymax: " + _child5.boundaryLeftBottomFront.Y.ToString());
                        Logger.Log("Zmin: " + _child5.boundaryLeftTopFront.Z.ToString() + " Zmax: " + _child5.boundaryLeftTopBack.Z.ToString());

                        Logger.Log("Child 6:");
                        Logger.Log("Xmin: " + _child6.boundaryLeftBottomFront.X.ToString() + " Xmax: " + _child6.boundaryRightBottomFront.X.ToString());
                        Logger.Log("Ymin: " + _child6.boundaryLeftTopFront.Y.ToString() + " Ymax: " + _child6.boundaryLeftBottomFront.Y.ToString());
                        Logger.Log("Zmin: " + _child6.boundaryLeftTopFront.Z.ToString() + " Zmax: " + _child6.boundaryLeftTopBack.Z.ToString());

                        Logger.Log("Child 7:");
                        Logger.Log("Xmin: " + _child7.boundaryLeftBottomFront.X.ToString() + " Xmax: " + _child7.boundaryRightBottomFront.X.ToString());
                        Logger.Log("Ymin: " + _child7.boundaryLeftTopFront.Y.ToString() + " Ymax: " + _child7.boundaryLeftBottomFront.Y.ToString());
                        Logger.Log("Zmin: " + _child7.boundaryLeftTopFront.Z.ToString() + " Zmax: " + _child7.boundaryLeftTopBack.Z.ToString());

                        Logger.Log("Child 8:");
                        Logger.Log("Xmin: " + _child8.boundaryLeftBottomFront.X.ToString() + " Xmax: " + _child8.boundaryRightBottomFront.X.ToString());
                        Logger.Log("Ymin: " + _child8.boundaryLeftTopFront.Y.ToString() + " Ymax: " + _child8.boundaryLeftBottomFront.Y.ToString());
                        Logger.Log("Zmin: " + _child8.boundaryLeftTopFront.Z.ToString() + " Zmax: " + _child8.boundaryLeftTopBack.Z.ToString());
                        Logger.Log("====================================================================");
                        //Logger.Log("Point error " + Logger.count.ToString());
                        //throw new System.IndexOutOfRangeException("Point doesn't belong to any octree child, cannot continue.");
                    }
                }
            }
            else
            {
                _child1.Split();
                _child2.Split();
                _child3.Split();
                _child4.Split();
                _child5.Split();
                _child6.Split();
                _child7.Split();
                _child8.Split();
            }
        }

        public FastCloud()
        {
            this._parent = null;
        }

        public FastCloud(FastCloud parent)
        {
            _parent = parent;
        }

        public void Insert(List<CloudPoint> points)
        {
            _points = points;

            double maxX = 0;
            double maxY = 0;
            double maxZ = 0;

            double minX = 0;
            double minY = 0;
            double minZ = 0;

            foreach (CloudPoint point in _points)
            {
                if (point.Position.X > maxX)
                    maxX = point.Position.X;

                if (point.Position.Y > maxY)
                    maxY = point.Position.Y;

                if (point.Position.Z > maxZ)
                    maxZ = point.Position.Z;

                if (point.Position.X < minX)
                    minX = point.Position.X;

                if (point.Position.Y < minY)
                    minY = point.Position.Y;

                if (point.Position.Z < minZ)
                    minZ = point.Position.Z;
            }
            
            boundaryLeftBottomFront = new Point3D(minX, maxY, minZ);
            boundaryRightBottomFront = new Point3D(maxX, maxY, minZ);
            boundaryLeftBottomBack = new Point3D(minX, maxY, maxZ);
            boundaryRightBottomBack = new Point3D(maxX, maxY, maxZ);

            boundaryLeftTopFront = new Point3D(minX, minY, minZ);
            boundaryRightTopFront = new Point3D(maxX, minY, minZ);
            boundaryLeftTopBack = new Point3D(minX, minY, maxZ);
            boundaryRightTopBack = new Point3D(maxX, minY, maxZ);

            
            _boundaries.Add(boundaryLeftBottomFront);
            _boundaries.Add(boundaryRightBottomFront);
            _boundaries.Add(boundaryLeftBottomBack);
            _boundaries.Add(boundaryRightBottomBack);
            _boundaries.Add(boundaryLeftTopFront);
            _boundaries.Add(boundaryRightTopFront);
            _boundaries.Add(boundaryLeftTopBack);
            _boundaries.Add(boundaryRightTopBack);
            
        }

        public void CalculateBoundaries()
        {
            this.Insert(this._points);
        }

        public List<CloudPoint> Points
        {
            get
            {
                return _points;
            }
        }

        public List<Point3D> Boundaries
        {
            get
            {
                return _boundaries;
            }
            set
            {
                _boundaries = value;
            }
        }

        public void PutBoundaries()
        {
            _boundaries.Add(boundaryLeftBottomFront); // 0
            _boundaries.Add(boundaryRightBottomFront); // 1
            _boundaries.Add(boundaryLeftBottomBack); // 2
            _boundaries.Add(boundaryRightBottomBack); // 3
            _boundaries.Add(boundaryLeftTopFront); // 4
            _boundaries.Add(boundaryRightTopFront); // 5
            _boundaries.Add(boundaryLeftTopBack); // 6
            _boundaries.Add(boundaryRightTopBack); // 7
        }

        private bool belongsToChild(CloudPoint point, FastCloud child)
        {
            //Check if belongs by X coords
            if (
                point.Position.X >= child.boundaryLeftBottomFront.X &&
                point.Position.X <= child.boundaryRightBottomFront.X
                )
            {
                // Check if belongs by Y coords
                if (
                    point.Position.Y >= child.boundaryLeftTopFront.Y &&
                    point.Position.Y <= child.boundaryLeftBottomFront.Y
                    )
                {
                    // Check if belongs by Z coords
                    if (
                        point.Position.Z >= child.boundaryLeftTopFront.Z &&
                        point.Position.Z <= child.boundaryLeftTopBack.Z
                        )
                    {
                        return true;
                    }
                }
            }
            
            return false;
        }

        public bool HasNoChildren()
        {
            if (_child1 == null)
                return true;

            return false;
        }

        public Cloud MakeCloud()
        {
            CloudChunk newChunk = new CloudChunk();
            newChunk.Color = Colors.Blue;
            Cloud newCloud = new Cloud();

            if (this.HasNoChildren())
            {
                newChunk.Points.AddRange(_points);
            }
            else
            {
                newChunk.Points.AddRange(_child1.MakeCloud().Chunks.ElementAt(0).Points);
                newChunk.Points.AddRange(_child2.MakeCloud().Chunks.ElementAt(0).Points);
                newChunk.Points.AddRange(_child3.MakeCloud().Chunks.ElementAt(0).Points);
                newChunk.Points.AddRange(_child4.MakeCloud().Chunks.ElementAt(0).Points);
                newChunk.Points.AddRange(_child5.MakeCloud().Chunks.ElementAt(0).Points);
                newChunk.Points.AddRange(_child6.MakeCloud().Chunks.ElementAt(0).Points);
                newChunk.Points.AddRange(_child7.MakeCloud().Chunks.ElementAt(0).Points);
                newChunk.Points.AddRange(_child8.MakeCloud().Chunks.ElementAt(0).Points);
            }

            newCloud.AddChunk(newChunk);
            return newCloud;
        }

        public int CountPoints()
        {
            int count = 0;

            if (_child1 == null)
            {
                count =  _points.Count();
            }
            else
            {
                count += _child1.CountPoints();
                count += _child2.CountPoints();
                count += _child3.CountPoints();
                count += _child4.CountPoints();
                count += _child5.CountPoints();
                count += _child6.CountPoints();
                count += _child7.CountPoints();
                count += _child8.CountPoints(); 
            }

            return count;
        }

        public int CountChildren()
        {
            int count = 0;

            if (_child1 == null)
            {
                return 8;
            }
            else
            {
                count += _child1.CountChildren();
                count += _child2.CountChildren();
                count += _child3.CountChildren();
                count += _child4.CountChildren();
                count += _child5.CountChildren();
                count += _child6.CountChildren();
                count += _child7.CountChildren();
                count += _child8.CountChildren();
            }

            return count;
        }

        public void SimplifyPoints()
        {
            if (_child1 == null)
            {
                if (_points.Count > 0)
                {
                    double averageX = 0;
                    double averageY = 0;
                    double averageZ = 0;

                    foreach (CloudPoint point in _points)
                    {
                        averageX += point.Position.X;
                        averageY += point.Position.Y;
                        averageZ += point.Position.Z;
                    }


                    Point3D centerOfMass = new Point3D(averageX / _points.Count, averageY / _points.Count, averageZ / _points.Count);
                    _points.Clear();
                    _points.Add(new CloudPoint(centerOfMass, 1));
                }
            }
            else
            {
                _child1.SimplifyPoints();
                _child2.SimplifyPoints();
                _child3.SimplifyPoints();
                _child4.SimplifyPoints();
                _child5.SimplifyPoints();
                _child6.SimplifyPoints();
                _child7.SimplifyPoints();
                _child8.SimplifyPoints();
            }
        }

        public IEnumerator GetEnumerator()
        {
            return (IEnumerator)_points.GetEnumerator();
        }
    }
}
