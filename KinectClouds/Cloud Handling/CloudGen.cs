﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Media3D;
using HelixToolkit.Wpf;
using System.Windows.Media;
using KinectClouds.Cloud_Engine;

namespace KinectClouds.Cloud_Handling
{
    class CloudGen
    {
        private Cloud _newCloud = new Cloud();
        private Random random = new Random();
        public Cloud GenerateCloud(int size, int chunks, int maxOffset, int minOffset)
        {
            for (int i = 0; i < chunks; i++)
            {
                CloudChunk newChunk = new CloudChunk();
                int color = random.Next(0, 5);

                switch (color)
                {
                    case 0:
                        newChunk.Color = Colors.Red;
                        break;
                    case 1:
                        newChunk.Color = Colors.Blue;
                        break;
                    case 2:
                        newChunk.Color = Colors.Orange;
                        break;
                    case 3:
                        newChunk.Color = Colors.Yellow;
                        break;
                    case 4:
                        newChunk.Color = Colors.Magenta;
                        break;
                    case 5:
                        newChunk.Color = Colors.Lime;
                        break;
                    default:
                        newChunk.Color = Colors.Black;
                        break;
                }

                for (int j = 0; j <= size / chunks; j++)
                {
                    Point3D randomCoordinates = new Point3D(random.Next(minOffset, maxOffset), random.Next(minOffset, maxOffset), random.Next(0, maxOffset));
                    CloudPoint newPoint = new CloudPoint(randomCoordinates, random.Next(-1000, 1000) / 10000);

                    newChunk.AddPoint(newPoint);
                }

                _newCloud.AddChunk(newChunk);
            }

            return _newCloud;
        }
    }
}
