﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;


//Delete this later, compiler loses shit because of this
namespace KinectClouds.Controls { }

namespace KinectClouds.Settings
{
    public class Settings
    {
        public bool userSitting { get; set; }
        public bool firstRun { get; set; }
        public bool positionSelectionAtStart { get; set; }


        public Settings()
        {
            userSitting = true;
            firstRun = true;
            positionSelectionAtStart = true;
        }

        public void Save()
        {
            using (var writer = new System.IO.StreamWriter("config.xml"))
            {
                var serializer = new XmlSerializer(this.GetType());
                serializer.Serialize(writer, this);
                writer.Flush();
            }
        }

        public static Settings Load()
        {
            try
            {
                using (var stream = System.IO.File.OpenRead("config.xml"))
                {
                    var serializer = new XmlSerializer(typeof(Settings));
                    return serializer.Deserialize(stream) as Settings;
                }
            }
            catch
            {
                KinectClouds.Settings.Settings newFile = new KinectClouds.Settings.Settings();
                newFile.Save();
                return KinectClouds.Settings.Settings.Load();
            }
        }
    }
}
